# Get installation file
#wget https://github.com/jetstack/cert-manager/releases/download/v1.5.0/cert-manager.yaml

# install kustomize
#curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash

# install cert-manager
k apply -f ./deploy/offline
